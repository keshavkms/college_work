


## Dataset
We wanted to work with Indian Music data which meant that access to proper information was harder to come by. India has a few sources like the Billboard top 100 which can also provide the historical trends for their rankings. Most Indian Charts are updated with no reverence to trends or history. It's almost like *cough* Their rankings are meant for populatiry and not *cough* Musical bravado. Which is a sad state of affairs considering the most talented artists seldom making it to top charts but we're not here to judge the sorry state of the industry, but rather understand what makes the industry tick in a way. 
Getting proper data was further complicated from the fact that a good percentage of Indian Audience depends on Youtube for their music consumption, where actual popularity is skewed by ...*sigh* item numbers and other pop *cough* bullshit but we're not here to judge. Atleast we're trying very hard not to. If that's what the public deems worthy then that's what we'll study. 

Probable sources of Data:
* Radio Station Lists
	* The data can have editorial prunings and can be a little more subjective in nature.
	* The lists usually only list top 20 on a weekly basis, and a more consolidated lists over larger time frames
	* They usually do not keep an archive of their lists 
* Editorials
	* Usually cover a wider timeline of consolidated data 
	* They are usually infrequent 
	* Editorial differences
* Youtube
	* A vast library of songs with niche interests as well
	* Provides analytics on song popularity and view counts as a function of time
	* The songs are not purely audio on the platform and hence the quality of videos also have a role to play in a songs populatiry,worst offenders being Item songs

* Gaana/Saavn and other streaming Services
	* Wide variety of songs with easy accesibility to people
	* New Entries into the market, and hence skew data in favor of newer songs

Best Deemed Approach :
We Settled on using radio station lists from two sources, Radio Mirchi and Red FM as these are the biggest and oldest radio channels in India and have been putting out a weekly list for a while. We had to painstakingly scrape data from archive.org to view their lists from 2008 onwards. 

Cons to the approach: 
* Due to the nature of archive.org , we couldn't get rankings from every week since 2008 .
* The data was sparse for the purposes of Machine learning and had a heavy bias on popular songs, with really no suitable representation of mediocre or low quality songs. So in totality we'd be counting on training on upper middle class of mediocre songs ( The songs that stayed on the low end of the rankings for a short amount of time ) to the "elite" hit songs ( Songs that stayed on top end of rankings for a long period of time ) which is understandably biased, but in some sense , also mimics our Idea of great songs as the songs that stay in memory for a longer time ( A subjective conclution we drew after looking at the data ) 

What can be experimented on:
A particularly interesting idea we had was to comb through how editorials on how top voted songs changed over time , to account for the bias against the newer songs on their lists and aggregating them across multiple editorials to see commonalities. We could also have done this analysis by genre as genre based listings are usually more up to data and comparable within their domain as opposed to generic " Top 100 songs of all time ", a particurly good example being Rock n' Roll magazines listings.


## Annotating the Dataset
Finding a good metric for song popularity is difficult due to the inherent subjectiveness of the problem. Any aggregate will stand a chance to lose out on regional popularity and other demographics which can be an important indicator. The medium of distribution will also play a role in the metric of songs popularity as certain mediums are more popular with certain demographics.

Even if we consider a medium to be roughly homogenous in its appeal, we get to the problem of quantifying variability. A song can be popular for reasons entirely outside the realm of the song due to aspects like marketing, Use in popular medium ,Popularity of said content, and a roll over factor that from recommendations. 

Songs that consistently score high on the metric over time can said to be bigger hits because time frame reduces the influence of concurrent factors, But it hurts newer songs more as they haven't been tested against time. 

The final metric and data used must reflect these assumptions.

## Feature Extraction

There are a lot of aspects of a song that can be used , For the analysis , We can narrow it down to averaged metrics, and Instantaneous Metrics
* Spectogram
* Self Similarity Matrices

Best Deemed Approach :

We ended up on deciding to use log scaled spectrogram as our input feature as it can be argued to be a rather complete representation of the song while scaling the data in a way that simulates our hearing in a way ( The log scaling ensures the Model interprets relative changes in the frequency, not absolute changes ).

Cons of the Approach :

Admittedly this is a little ham-fisted approach , trying to throw everything at the model and seeing if it works, But I'll defend the idea in the name of exploration and curiosity as we wanted to see if there were factors that we might not have thought of , patterns hidden in the song that couldn't be solely defined or even identified in the high level semantics of the song. 

Ideas to be experimented on :

We'd like to find a way to imbue the model with our current learnings in a way that we dont bias it against finding new patterns too much, but make the prediction more accurate and in line with our current understanding. One way that we can consider is weighing the input dynamically in response to the loss function, a technique which is being researched in the domain of computer vision, the broad strokes of the idea being that given an image of an object, what features are necessary to define the object. If we can identify wheels and a red body, is it a fire-truck? If not, what other features can we use it to differentiate between other red-bodied and wheeled objects. We can use a similar techniques to analyse music where we can stick to known concepts when they provide an adequate explaination, only going for nuances and minutia when the explaination is deemed inadequate.

## Model Selection
We wanted to choose a model that could work with low level data because including higher level features would be distorted by the semantic gap within the confines of the model and we may not have been able to discern the differences. So we settled on looking for low level factors that could have been common to hit songs. not because those results would have been more accurate , but because correlations could be more easily identified and they'd be mostly be untainted by concious perception. 

### Regression based models
{{ @Ananya Fill within the lines marked. Leave the best deemed approach etc etc , I'll fill that }}
__________________________

* CNN
* Logistic Regression
### Classification based models
Although we had to perform some sort of regression on the data, We could get some useable information by looking at the discrete classification as well. 
* SVM
* Bayes Classifier
____________________
Best Deemed Approach :

Cons to the Selected Approach :

What can be experimented on :

## Pruning


## Loss Function / Fitness Evaluation
Designing a suitable loss function 


Best Deemed Approach

Cons of the Selected Approach:

Ideas to be experimented on :

## Validation

Best Deemed Approach :

Cons of the Approach :

Ideas to be experimented on :

## Conclusion 
